# ppw-lab-new
[![pipeline status](https://gitlab.com/ihsanaulia/ppwlabnew/badges/master/pipeline.svg)](https://gitlab.com/ihsanaulia/ppwlabnew/commits/master)
[![coverage report](https://gitlab.com/ihsanaulia/ppwlabnew/badges/master/coverage.svg)](https://gitlab.com/ihsanaulia/ppwlabnew/commits/master)

Repo baru untuk belajar TDD dengan django.

[Link App Heroku](https://ppwtdd.herokuapp.com)
