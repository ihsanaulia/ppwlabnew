from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from .forms import StatusForm, SubscriberForm
from .models import Status, Subscribe
import requests
from django.http import JsonResponse
from django.db import IntegrityError
import json
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
response = {}
responseForm = {}

def index(request):
    allstatus = Status.objects.all().order_by('-created_date')
    response['all_status'] = allstatus
    response['status_form'] = StatusForm
    return render(request, "home.html", response)

def post_status(request):
    if request.method == 'POST' and 'submit' in request.POST:
        response['title'] = request.POST['title']
        response['status'] = request.POST['status']
        status_save = Status(title=response['title'], status=response['status'])
        status_save.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')

def lib(request):
    return render(request, "lib.html")

def jsonBook(request, pick=''):
    url = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + {pick})
    jsons = url.json()
    return JsonResponse(jsons)

def bio(request):
    return render(request, "bio.html")

def subs(request):
    form = SubscriberForm(request.POST or None)
    responseForm['form'] = form
    return render(request, 'subscribe.html', responseForm)

def submit(request):
    try:
        if (request.method == 'POST'):
            name = request.POST.get('name', None)
            email = request.POST.get('email', None)
            password = request.POST.get('password', None)
            Subscribe(name=name, email=email, password=password).save()
            return HttpResponse(json.dumps({'result': 'Registration is completed'}))
        else:
            return HttpResponse(json.dumps({'result': 'Registration failed, please try again'}))
    except IntegrityError as error:
        return HttpResponse(json.dumps({'result': 'Registration failed, please try again'}))

def check_email(request):
    mail = request.POST.get('email', None)
    data = {
        'already_registered': Subscribe.objects.filter(email=mail).exists()
    }
    return JsonResponse(data)

