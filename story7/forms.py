from django import forms
from .models import *

class StatusForm(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    title = forms.CharField(label = 'Title', required = True, widget=forms.TextInput(attrs=attrs), max_length=30)
    status = forms.CharField(label = 'Status', required = True, widget=forms.TextInput(attrs=attrs), max_length=300)

    class Meta:
        model = Status

class SubscriberForm(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    name = forms.CharField(label="Name", required=True, max_length=100, widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label="Email", required=True, max_length=50, widget=forms.EmailInput(attrs=attrs))
    password = forms.CharField(label="Password", required=True, max_length=150, widget=forms.PasswordInput(attrs=attrs))

    class Meta:
        model = Subscribe
