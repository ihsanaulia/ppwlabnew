from django.db import models

# Create your models here.
class Status(models.Model):
    title = models.CharField(max_length=20)
    status = models.CharField(max_length=300)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

class Subscribe(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=50, unique=True)
    password = models.CharField(max_length=150)

    def __str__(self):
        return self.name
