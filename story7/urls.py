from django.urls import path, re_path
from .views import index, lib, jsonBook, bio, subs, submit, check_email
from .views import post_status
#this is a comment
urlpatterns = [
    path('', bio, name='bio'),
    re_path('test/', post_status, name='post_status'),
    path('books/', lib, name='lib'),
    path('data/', jsonBook, name='data'),
    path('status/', index, name='index'),
    path('subscribe/', subs, name='subscribe'),
    re_path('subscribe/submit', submit, name='submit'),
    re_path('subscribe/checkemail', check_email, name='check_email')
]
